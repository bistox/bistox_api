FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git make protobuf openssh-client build-base

ARG ssh_prv_key
RUN mkdir /root/.ssh && echo "${ssh_prv_key}" > /root/.ssh/id_rsa && \
    chmod 0600 /root/.ssh/id_rsa && \
    eval $(ssh-agent) && \
    echo -e "StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
    ssh-add /root/.ssh/id_rsa
RUN git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

RUN go get -u -v gitlab.com/bistox/bistox_api 2>&1

ARG reset_cache
RUN for repo in /go/src/gitlab.com/bistox/*; do cd $repo && git pull -v --progress && cd ..; done 2>&1

WORKDIR /go/src/gitlab.com/bistox/bistox_api

ENV GOSRC=/go/src
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags '-w -s -extldflags "-static"' -o /go/bin/bistox_api

FROM scratch
COPY --from=builder /go/bin/bistox_api /
COPY --from=builder /go/src/gitlab.com/bistox/bistox_api/bistox.yaml /
ENTRYPOINT ["/bistox_api"]
