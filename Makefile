GOPATH=${HOME}/go
GOSRC=${GOPATH}/src
GOBIN=${GOPATH}/bin
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
PROTO=${GOSRC}/gitlab.com/bistox/bistox_proto
BINARY_NAME=bin/bistox_api

all: protogen build
build:
	$(GOBUILD) -o $(BINARY_NAME) -v
test:
	$(GOTEST) -v ./...
clean:
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
run:
	$(GOBUILD) -o $(BINARY_NAME) -v ./...
	./$(BINARY_NAME)
deps:
	$(GOGET) -d ./...
	$(GOCMD) get -u github.com/golang/protobuf/protoc-gen-go
	$(GOCMD) get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	$(GOCMD) get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
protogen:
	protoc -I ${GOSRC} \
        --go_out=plugins=grpc:${GOSRC} \
        ${PROTO}/common.proto
	protoc -I ${GOSRC} \
        --go_out=plugins=grpc:${GOSRC} \
        ${PROTO}/payserver/payserver.proto
	protoc -I ${GOSRC} \
        --go_out=plugins=grpc:${GOSRC} \
        ${PROTO}/engine/engine.proto
	protoc -I ${GOSRC} \
        -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
        --go_out=plugins=grpc:${GOSRC} \
        ${PROTO}/api/api.proto
	protoc -I ${GOSRC} \
        -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
        --grpc-gateway_out=logtostderr=true:${GOSRC} \
        ${PROTO}/api/api.proto
	protoc -I ${GOSRC} \
        -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
        --swagger_out=logtostderr=true:${GOSRC} \
        ${PROTO}/api/api.proto

build-packed:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o $(BINARY_NAME) -v
docker-build:
	docker run --rm -it -v "$(GOPATH)":/go -w /go/src/gitlab.org/... golang:latest go build -o "$(BINARY_NAME)" -v
