package main

import (
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_api/handlers"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/server"
	pb "gitlab.com/bistox/bistox_proto/api"
	"google.golang.org/grpc"
	"os"
)

//go:generate make protogen


var log = logging.MustGetLogger("bistox")


func main() {
	service := bistox_core.NewService(logging.DEBUG)
	service.Init()

	apiServer := handlers.NewApiServer(&service)

	service.Config.Server.Ext = server.ConfigExt{
		RpcRegHandler: func(s *grpc.Server, mux *runtime.ServeMux) {
			pb.RegisterBistoxApiServer(s, &apiServer)
		},
		GwRegHandlers: []server.GwRegHandler{
			pb.RegisterBistoxApiHandlerFromEndpoint,
		},
		ServerOptions: []grpc.ServerOption{
			server.AuthUnaryServerInterceptor(service.Config.Server.Options),
		},
	}

	if err := service.Run(); err != nil {
		os.Exit(1)
	}
}
