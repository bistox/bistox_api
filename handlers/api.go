package handlers

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"gitlab.com/bistox/bistox_proto"
	api "gitlab.com/bistox/bistox_proto/api"
	engine "gitlab.com/bistox/bistox_proto/engine"
	payServer "gitlab.com/bistox/bistox_proto/payserver"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
	"time"
)

const HandlerMaxTime = 2000 * time.Millisecond
var log = logging.MustGetLogger("bistox")


type ApiServer struct {
	service *bistox_core.Service
	payClients map[models.CryptoCurrency]payServer.BistoxPayserverClient
	engineClient engine.BistoxEngineClient
}


func (s *ApiServer) SignUp(ctx context.Context, in *api.SignUpRequest) (*api.SignUpResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)

	profile := models.Profile{Login: in.Email, Password: in.Password, Email: in.Email}

	if err := profile.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	profile.HashPassword()

	err := db.RunInTransaction(func(tx *pg.Tx) error {
		_, err := tx.
			Model(&profile).
			OnConflict("do nothing").
			Insert(&profile)

		if err != nil {
			log.Error(err)
			return status.Error(codes.Internal, "Internal error")
		}

		if profile.ID == "" {
			return status.Error(codes.AlreadyExists, "Profile already exists")
		}

		for _, currency := range models.CryptoCurrencies {
			ctx = context.WithValue(ctx, "AccountId", profile.ID)
			ctx = context.WithValue(ctx, "Tx", tx)
			if _, err := s.CreateWallet(ctx, &api.CreateWalletRequest{
				Name: strings.ToLower(string(currency)) + "_default", Currency: currency.ToProto(),
			}); err != nil {
				return err
			}
		}

		if _, err := s.CreateWallet(ctx, &api.CreateWalletRequest{
			Name: "usd_fiat", Currency: models.USD.ToProto(),
		}); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	ss, err := s.service.Server().GetNewToken(profile.ID)

	if err != nil {
		return nil, err
	}

	log.Debugf("User %s successfully created!", profile.Login)
	return &api.SignUpResponse{Id: profile.ID, Token: ss, Email: profile.Email, Login: profile.Login}, nil
}


func (s *ApiServer) SignIn(ctx context.Context, in *api.SignInRequest) (*api.SignInResponse, error) {
	profile := models.Profile{Login: in.Login}
	err := s.service.DB().
		Model((*models.Profile)(nil)).
		Column("password", "id", "email", "login").
		Where("email = ? and deleted_at is null", in.Login).
		Select(&profile)
	if err == pg.ErrNoRows {
		return nil, status.Error(codes.Unauthenticated, "Invalid login or password")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if !profile.CheckPassword(in.Password) {
		return nil, status.Error(codes.Unauthenticated, "Invalid login or password")
	}

	ss, err := s.service.Server().GetNewToken(profile.ID)

	if err != nil {
		return nil, err
	}

	return &api.SignInResponse{Token: ss, Login: profile.Login, Email: profile.Email}, nil
}


func (s *ApiServer) GetProfile(ctx context.Context, request *proto.EmptyRequest) (*api.Profile, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)
	profile := models.Profile{BaseModel: models.BaseModel{ID: profileId}}

	err := db.Model(&profile).
		Column("password", "email", "login").
		Where("id = ?id").
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Error(codes.NotFound, "Profile is not found")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	return &api.Profile{
		Login: profile.Login,
		Email: profile.Email,
		Settings: map[string]string{},
	}, nil
}


func (s *ApiServer) UpdateProfile(ctx context.Context, profileNewData *api.Profile) (*proto.EmptyResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)
	profile := models.Profile{BaseModel: models.BaseModel{ID: profileId}}

	err := db.Model(&profile).
		Column("password", "id", "email", "login").
		Where("id = ?id").
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Error(codes.NotFound, "Profile is not found")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if profileNewData.Login != "" {
		profile.Login = profileNewData.Login
	}

	// Check login conflict
	err = db.Model(&models.Profile{Login: profileNewData.Login}).
		Where("login = ?login").
		Column("id").
		Select()

	if err == pg.ErrNoRows {
		// ok!
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	} else {
		return nil, status.Error(codes.InvalidArgument, "Login already exists")
	}

	_, err = db.Model(&profile).
		Set("login = ?login").
		Where("id = ?id").
		Update()

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &proto.EmptyResponse{}, nil
}


func (s *ApiServer) ChangePassword(ctx context.Context, in *api.ChangePasswordRequest) (*proto.EmptyResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)
	profile := models.Profile{BaseModel: models.BaseModel{ID: profileId}}

	err := db.Model(&profile).
		Column("password", "id").
		Where("id = ?id").
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Error(codes.NotFound, "Profile is not found")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if !profile.CheckPassword(in.OldPassword) {
		return nil, status.Error(codes.InvalidArgument, "Invalid old password")
	}

	// replace with profile.validate()
	if in.NewPassword == "" {
		return nil, status.Error(codes.InvalidArgument, "Invalid new password")
	}

	profile.Password = in.NewPassword
	profile.HashPassword()

	_, err = db.Model(&profile).
		Set("password = ?password").
		Where("id = ?id").
		Update()

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &proto.EmptyResponse{}, nil
}


// Return a new token if given token is valid
func (s *ApiServer) TokenExtend(ctx context.Context, in *api.TokenExtendRequest) (*api.TokenExtendResponse, error) {

	accountId := ctx.Value("AccountId").(string)

	log.Debugf("Token extend for account with id: %v", accountId)

	ss, err := s.service.Server().GetNewToken(accountId)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Invalid token")
	}

	return &api.TokenExtendResponse{Token: ss}, nil
}


func NewApiServer(service *bistox_core.Service) ApiServer {
	payServers := service.Config.GetMap("payserver")
	payClients := make(map[models.CryptoCurrency]payServer.BistoxPayserverClient, len(payServers))
	for currency, address := range payServers {
		conn, _ := grpc.Dial(address.(string), grpc.WithInsecure())
		payClients[models.CryptoCurrency(strings.ToUpper(currency.(string)))] = payServer.NewBistoxPayserverClient(conn)
	}

	engineAddr := service.Config.GetString("engine", "addr")
	engineConn, _ := grpc.Dial(engineAddr, grpc.WithInsecure())
	engineClient := engine.NewBistoxEngineClient(engineConn)

	return ApiServer{service: service, payClients: payClients, engineClient: engineClient}
}
