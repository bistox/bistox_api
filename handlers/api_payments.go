package handlers

import (
	"context"
	"github.com/go-pg/pg"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"gitlab.com/bistox/bistox_proto"
	api "gitlab.com/bistox/bistox_proto/api"
	payServer "gitlab.com/bistox/bistox_proto/payserver"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"math"
)


// + Permissions
func (s *ApiServer) getWallet(ctx context.Context, walletId string) (*models.Wallet, bool, error) {
	wallet := &models.Wallet{
		ProfileId: ctx.Value("AccountId").(string),
		BaseModel: models.BaseModel{ID: walletId},
	}

	err := s.service.DB().Model(wallet).
		Where("profile_id = ?profile_id and id = ?id").
		Column("currency_code").
		Select()

	if err == pg.ErrNoRows {
		return nil, false, nil
	} else if err != nil {
		return nil, false, status.Error(codes.Internal, "Internal error")
	}

	return wallet, true, nil
}


func (s *ApiServer) Payout(ctx context.Context, in *api.PayoutRequest) (*proto.Payment, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	wallet, access, err := s.getWallet(ctx, in.WalletId)
	if err != nil {
		log.Error(err)
		return nil, err
	} else if !access {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	}

	amount := int64(math.Abs(float64(models.Divisor[wallet.CurrencyCode]) * in.Amount))

	if amount < 1000 {
		return nil, status.Error(codes.InvalidArgument, "Too low amount")
	}

	payment, err := s.payClients[wallet.CurrencyCode].CreatePayment(ctx, &payServer.PaymentRequest{
		Amount: amount,
		ExtAddress: in.ExtAddress,
		Currency: wallet.CurrencyCode.ToProto(),
		WalletId: wallet.ID,
		Direction: proto.PaymentDirection_OUTGOING,
	})

	return payment, err
}


func (s *ApiServer) FiatPayin(ctx context.Context, in *api.FiatPayinRequest) (*proto.EmptyResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	wallet, access, err := s.getWallet(ctx, in.WalletId)
	if err != nil {
		log.Error(err)
		return nil, err
	} else if !access {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	} else if wallet.CurrencyCode != models.USD {
		return nil, status.Error(codes.InvalidArgument, "NotFiatWallet")
	}

	amount := int64(math.Abs(float64(models.Divisor[wallet.CurrencyCode]) * in.Amount))

	if amount == 0 {
		return nil, status.Error(codes.InvalidArgument, "AmountLessOrEqualZero")
	}

	_, err = s.service.DB().Model(wallet).
		Set("balance = balance + ?", amount).
		Where("id = ?id").
		Update()

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &proto.EmptyResponse{}, nil
}


// FIXME: Dummy function (DEMO)
func (s *ApiServer) BuyCrypto(ctx context.Context, in *api.BuyCryptoRequest) (*proto.EmptyResponse, error) {
	fromWallet, fromAccess, fromErr := s.getWallet(ctx, in.WalletId)
	toWallet, toAccess, toErr := s.getWallet(ctx, in.ToWalletId)

	if fromErr != nil || toErr != nil {
		log.Error(fromErr, toErr)
		return nil, status.Error(codes.Internal, "Internal error")
	} else if !fromAccess || !toAccess {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	} else if fromWallet.CurrencyCode != models.USD {
		return nil, status.Error(codes.InvalidArgument, "NotFiatWalletFrom")
	} else if toWallet.CurrencyCode == models.USD {
		return nil, status.Error(codes.InvalidArgument, "NotCryptoWalletTo")
	}

	cryptoPrices := map[models.CurrencyEnum]float64{
		models.ETH: 95.11,
		models.BTC: 3539.24,
	}

	// DEMO
	fromAmount := -int64(float64(models.Divisor[fromWallet.CurrencyCode]) * cryptoPrices[toWallet.CurrencyCode] * math.Abs(in.Amount))

	// DEMO
	toAmount := int64(float64(models.Divisor[toWallet.CurrencyCode]) * math.Abs(in.Amount))

	if err := s.service.DB().RunInTransaction(func(tx *pg.Tx) error {
		if _, err := tx.Model(fromWallet).
			Set("balance = balance + ?", fromAmount).
			Where("id = ?id").
			Returning("balance").
			Update(); err != nil { return err }

		if fromWallet.Balance < 0 {
			return status.Error(codes.Canceled, "LowBalance")
		}

		if _, err := s.payClients[toWallet.CurrencyCode].CreatePayment(ctx, &payServer.PaymentRequest{
			Amount: toAmount,
			Currency: toWallet.CurrencyCode.ToProto(),
			WalletId: toWallet.ID,
			Direction: proto.PaymentDirection_INTERNAL,
		}); err != nil {return err }

		return nil
	}); err != nil {
		log.Error(err)
		return nil, err
	}

	return &proto.EmptyResponse{}, nil
}
