package handlers

import (
	"context"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	common "gitlab.com/bistox/bistox_proto"
	api "gitlab.com/bistox/bistox_proto/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
)


func (s *ApiServer) GetHistoryChart(ctx context.Context, req *api.HistoryChartRequest) (*api.HistoryChart, error) {
	db := s.service.DB()

	var bars []*api.HistoryChartBar

	resolution := strings.ToLower(req.TimeResolution.String())

	if req.BaseCurrency == common.Currency_CURRENCY_NONE || req.QuotedCurrency == common.Currency_CURRENCY_NONE {
		return nil, status.Error(codes.InvalidArgument, "InvalidCurrency")
	}

	baseCurrency, quotedCurrency := models.ProtoToCurrencyEnum(req.BaseCurrency), models.ProtoToCurrencyEnum(req.QuotedCurrency)
	divisor := models.Divisor[quotedCurrency]

	err := db.Model((*models.TradeHistory)(nil)).
		ColumnExpr("extract(epoch from(date_trunc(?, created_at))) as time", resolution).
		ColumnExpr("max(price) / ?. as high", divisor).
		ColumnExpr("min(price) / ?. as low", divisor).
		ColumnExpr("(array_agg(price order by created_at))[1] / ?. as open", divisor).
		ColumnExpr("(array_agg(price order by created_at desc))[1] / ?. as close", divisor).
		Group("time").
		Order("time desc").
		Where("created_at <= to_timestamp(?)", req.ToTimestamp).
		Where("base_currency_code = ? and quoted_currency_code = ?", baseCurrency, quotedCurrency).
		Limit(int(req.Limit)).
		Select(&bars)

	for i, j := 0, len(bars) - 1; i < j; i, j = i + 1, j - 1 {
		bars[i], bars[j] = bars[j], bars[i]
	}

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &api.HistoryChart{ Data: bars}, nil
}


func (s *ApiServer) GetDepthChart(ctx context.Context, req *api.DepthChartRequest) (*api.DepthChart, error) {
	db := s.service.DB()

	var buyPart []*api.DepthChartPart
	var sellPart []*api.DepthChartPart
	var err error

	if req.BaseCurrency == common.Currency_CURRENCY_NONE || req.QuotedCurrency == common.Currency_CURRENCY_NONE {
		return nil, status.Error(codes.InvalidArgument, "InvalidCurrency")
	}

	baseCurrency, quotedCurrency := models.ProtoToCurrencyEnum(req.BaseCurrency), models.ProtoToCurrencyEnum(req.QuotedCurrency)
	divisor := models.Divisor[quotedCurrency]

	err = db.Model((*models.Order)(nil)).
		ColumnExpr("price/10000/10000. as price_val").
		ColumnExpr("count(*) as count").
		ColumnExpr("sum(amount) / ?. as amount", divisor).
		Group("price_val").
		Order("price_val").
		Where("operation_type = 'SELL' and order_type = 'LIMIT' and deleted_at is null").
		Where("base_currency_code = ? and quoted_currency_code = ?", baseCurrency, quotedCurrency).
		Limit(20).
		Select(&sellPart)

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	err = db.Model((*models.Order)(nil)).
		ColumnExpr("price/10000/10000. as price_val").
		ColumnExpr("count(*) as count").
		ColumnExpr("sum(amount) / ?. as amount", divisor).
		Group("price_val").
		Order("price_val desc").
		Where("operation_type = 'BUY' and order_type = 'LIMIT' and deleted_at is null").
		Where("base_currency_code = ? and quoted_currency_code = ?", baseCurrency, quotedCurrency).
		Limit(20).
		Select(&buyPart)

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &api.DepthChart{Buy: buyPart, Sell: sellPart}, nil
}


func (s *ApiServer) GetDealsChart(ctx context.Context, req *api.DealsChartRequest) (*api.DealsChart, error) {
	db := s.service.DB()

	var orders []*models.Order
	var outOrders []*common.Order

	if req.BaseCurrency == common.Currency_CURRENCY_NONE || req.QuotedCurrency == common.Currency_CURRENCY_NONE {
		return nil, status.Error(codes.InvalidArgument, "InvalidCurrency")
	}

	var limit uint32 = 20

	if req.Limit > 0 && req.Limit < 100 {
		limit = req.Limit
	}

	baseCurrency, quotedCurrency := models.ProtoToCurrencyEnum(req.BaseCurrency), models.ProtoToCurrencyEnum(req.QuotedCurrency)

	err := db.Model(&orders).
		Order("deleted_at desc").
		Where("deleted_at is not null").
		Where("base_currency_code = ? and quoted_currency_code = ?", baseCurrency, quotedCurrency).
		Limit(int(limit)).
		Select()

	for _, order := range orders {
		outOrders = append(outOrders, order.ToProto())
	}

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &api.DealsChart{Deals: outOrders}, nil
}


func (s *ApiServer) GetMarketInfo(ctx context.Context, req *common.EmptyRequest) (*api.MarketInfo, error) {
	db := s.service.DB()
	pairs := map[string]*api.PairMarketInfo{}

	var fiatPrices = map[models.CurrencyEnum]float64{
		models.ETH: 131.30,
		models.DASH: 93.33,
		models.BCH: 127.23,
		models.LTC: 55.70,
		models.BTC: 3853.38,
	}

	for _, pair := range models.ValidPairs {
		info := &api.PairMarketInfo{}
		err := db.Model((*models.TradeHistory)(nil)).
			ColumnExpr("(array_agg(price order by created_at desc))[1] / ?. as price", models.Divisor[pair.BaseCurrency]).
			ColumnExpr("min(price) / ?. as price_low", models.Divisor[pair.BaseCurrency]).
			ColumnExpr("max(price) / ?. as price_high", models.Divisor[pair.BaseCurrency]).
			ColumnExpr("avg(volume) / ?. as volume", models.Divisor[pair.BaseCurrency]).
			ColumnExpr(`
				((array_agg(price order by created_at desc))[1] - (array_agg(price order by created_at))[1])
				/ ?. as change`, models.Divisor[pair.BaseCurrency]).
			Where("created_at >= now() - interval '1 day'").
			Where("base_currency_code = ? and quoted_currency_code = ?", pair.BaseCurrency, pair.QuotedCurrency).
			Select(info)

		if err != nil {
			log.Error(err)
			return nil, status.Error(codes.Internal, "Internal error")
		}

		if fprice, ok := fiatPrices[pair.BaseCurrency]; pair.QuotedCurrency == models.USD && ok {
			info.Price = fprice
			info.PriceLow = fprice
			info.PriceHigh = fprice
		}

		pairs[string(pair.BaseCurrency) + "/" + string(pair.QuotedCurrency)] = info
	}

	return &api.MarketInfo{Pairs: pairs}, nil
}