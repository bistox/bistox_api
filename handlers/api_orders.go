package handlers

import (
	"context"
	"github.com/go-pg/pg"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"gitlab.com/bistox/bistox_proto"
	api "gitlab.com/bistox/bistox_proto/api"
	engine "gitlab.com/bistox/bistox_proto/engine"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func logAndReturnInternalError(err error) error {
	log.Error(err)
	return status.Error(codes.Internal, "Internal error")
}

func (s *ApiServer) CreateOrder(ctx context.Context, req *api.CreateOrderRequest) (*proto.Order, error) {
	log.Debug("Creating new order...")

	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)

	pair := models.CurrencyPair{
		BaseCurrency: models.ProtoToCurrencyEnum(req.BaseCurrency),
		QuotedCurrency: models.ProtoToCurrencyEnum(req.QuotedCurrency),
	}
	if !pair.IsValid() {
		return nil, status.Errorf(codes.InvalidArgument, "invalid currency pair: %s/%s",
			req.BaseCurrency.String(), req.QuotedCurrency.String())
	}

	if req.Amount < 0.000001 {
		return nil, status.Errorf(codes.InvalidArgument, "Order amount can't be too low")
	}

	if models.OrderType(req.OrderType) == models.Limit && req.Price < 0.000001 {
		return nil, status.Errorf(codes.InvalidArgument, "Limit order price can't too low")
	}

	baseWallet := new(models.Wallet)
	err := db.Model(baseWallet).
		Where("id = ? and deleted_at is null", req.BaseWalletId).
		Where("profile_id = ?", profileId).
		Where("currency_code = ?", req.BaseCurrency.String()).
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Errorf(codes.InvalidArgument, "%s wallet with id %s is not found",
			req.BaseCurrency.String(), req.BaseWalletId)
	} else if err != nil {
		return nil, logAndReturnInternalError(err)
	}

	quotedWallet := new(models.Wallet)
	err = db.Model(quotedWallet).
		Where("id = ? and deleted_at is null", req.QuotedWalletId).
		Where("profile_id = ?", profileId).
		Where("currency_code = ?", req.QuotedCurrency.String()).
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Errorf(codes.InvalidArgument, "%s wallet with id %s is not found",
			req.QuotedCurrency.String(), req.QuotedWalletId)
	} else if err != nil {
		return nil, logAndReturnInternalError(err)
	}

	intPrice := int64(req.Price * float64(models.Divisor[models.ProtoToCurrencyEnum(req.QuotedCurrency)]))
	intAmount := int64(req.Amount * float64(models.Divisor[models.ProtoToCurrencyEnum(req.BaseCurrency)]))

	if models.ProtoToOperationType(req.OperationType) == models.Buy {
		if models.ProtoToOrderType(req.OrderType) == models.Market {
			err = db.Model((*models.Order)(nil)).
				Column("price").
				Where("base_currency_code = ? and quoted_currency_code = ? and deleted_at is null",
					req.BaseCurrency.String(), req.QuotedCurrency.String()).
				Order("price ASC").
				Limit(1).
				Select(&intPrice)
			if err == pg.ErrNoRows {
				intPrice = 0
			} else if err != nil {
				return nil, logAndReturnInternalError(err)
			}
		}
		if quotedWallet.Balance == 0 || quotedWallet.Balance < intPrice * intAmount / models.Divisor[models.ProtoToCurrencyEnum(req.BaseCurrency)] {
			return nil, status.Errorf(codes.FailedPrecondition, "Not enough %s on balance", req.QuotedCurrency)
		}
	} else if models.ProtoToOperationType(req.OperationType) == models.Sell {
		if baseWallet.Balance == 0 || baseWallet.Balance < intAmount {
			return nil, status.Errorf(codes.FailedPrecondition, "Not enough %s on balance", req.BaseCurrency)
		}
	}

	engineRes, err := s.engineClient.CreateOrder(
		ctx,
		&engine.CreateOrderRequest{
			OrderType:     	req.OrderType,
			OperationType: 	req.OperationType,
			BaseCurrency:  	req.BaseCurrency,
			QuotedCurrency:	req.QuotedCurrency,
			Amount:        	intAmount,
			Price:         	intPrice,
			BaseWalletId:	req.BaseWalletId,
			QuotedWalletId: req.QuotedWalletId,
			ProfileId:		profileId,
		},
	)

	if err != nil {
		return nil, logAndReturnInternalError(err)
	} else {
		log.Debugf("Order %d was successfully created!", engineRes.OrderId)
	}

	return engineRes, nil
}

func (s *ApiServer) CancelOrder(ctx context.Context, req *api.CancelOrderRequest) (*api.CancelOrderResponse, error) {
	log.Debugf("Cancelling order %d...", req.OrderId)

	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)

	var order models.Order
	err := db.Model(&order).
		Where("id = ?", req.OrderId).
		Where("profile_id = ?", profileId).
		Where("deleted_at is null").
		Select()
	if err == pg.ErrNoRows {
		return nil, status.Errorf(codes.NotFound, "Active order %d was not found", req.OrderId)
	} else if err != nil {
		return nil, logAndReturnInternalError(err)
	}

	_, err = s.engineClient.CancelOrder(
		ctx,
		&engine.CancelOrderRequest{OrderId: req.OrderId},
	)
	if err != nil {
		return nil, logAndReturnInternalError(err)
	}

	return &api.CancelOrderResponse{}, nil
}

func (s *ApiServer) GetOrders(ctx context.Context, req *api.GetOrdersRequest) (*proto.Orders, error) {
	log.Debug("Getting orders...")

	return s.getOrders(ctx, req, false)
}

func (s *ApiServer) GetMyOrders(ctx context.Context, req *api.GetOrdersRequest) (*proto.Orders, error) {
	log.Debug("Getting my orders...")

	return s.getOrders(ctx, req, true)
}

func (s *ApiServer) getOrders(ctx context.Context, req *api.GetOrdersRequest, byProfile bool) (*proto.Orders, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)

	pair := models.CurrencyPair{
		BaseCurrency: models.ProtoToCurrencyEnum(req.BaseCurrency),
		QuotedCurrency: models.ProtoToCurrencyEnum(req.QuotedCurrency),
	}
	if !pair.IsValid() {
		return nil, status.Errorf(codes.InvalidArgument, "invalid currency pair: %s/%s",
			req.BaseCurrency.String(), req.QuotedCurrency.String())
	}

	var order string
	if req.OperationType == proto.OperationType_BUY {
		order = "price DESC"
	} else {
		order = "price ASC"
	}
	var orders []models.Order
	query := db.Model(&orders)

	if byProfile {
		profileId := ctx.Value("AccountId").(string)
		query = query.Where("profile_id = ?", profileId)
	}

	if req.OrderType != proto.OrderType_ORDER_TYPE_NONE {
		query = query.Where("order_type = ?", req.OrderType.String())
	}
	if req.OperationType != proto.OperationType_OPERATION_TYPE_NONE {
		query = query.Where("operation_type = ?", req.OperationType.String())
	}
	if req.BaseCurrency != proto.Currency_CURRENCY_NONE {
		query = query.Where("base_currency_code = ?", req.BaseCurrency.String())
	}
	if req.QuotedCurrency != proto.Currency_CURRENCY_NONE {
		query = query.Where("quoted_currency_code = ?", req.QuotedCurrency.String())
	}
	if req.Deleted {
		query = query.Where("deleted_at is not null")
	} else {
		query = query.Where("deleted_at is null")
	}

	err := query.Order(order).Limit(10).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, logAndReturnInternalError(err)
	}

	protoOrders := make([]*proto.Order, len(orders))
	for i, order := range orders {
		protoOrders[i] = order.ToProto()
	}

	return &proto.Orders{Orders: protoOrders}, nil
}