package handlers

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	api "gitlab.com/bistox/bistox_proto/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const AddressesPerWallet = 3


func (s *ApiServer) CreateWallet(ctx context.Context, in *api.CreateWalletRequest) (*api.Wallet, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)

	if in.Currency.String() == "ASSET_NONE" {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid asset %v", 1)
	}

	if in.Name == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid name %v", in.Name)
	}

	wallet := &models.Wallet{
		PrivateKey: []uint8{},
		ProfileId: profileId,
		Name: in.Name,
		CurrencyCode: models.CurrencyEnum(in.Currency.String()),
	}

	// Use existing tx
	var err error
	tx, isOuterTx := ctx.Value("Tx").(*pg.Tx)

	if !isOuterTx {
		tx, err = db.Begin()
		if err != nil {
			log.Error(err)
			return nil, status.Error(codes.Internal, "Internal error")
		}
		defer tx.Rollback()
	}

	_, err = tx.
		Model(wallet).
		OnConflict("(profile_id, name) do nothing").
		Insert(wallet)

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if wallet.ID == "" {
		return nil, status.Error(codes.AlreadyExists, "Wallet with that name already exists")
	}

	if wallet.CurrencyCode != models.USD {
		wallet.Addresses = make([]*models.CryptoAddress, AddressesPerWallet)
		for i := 0; i < AddressesPerWallet; i++ {
			address := &models.CryptoAddress{
				CurrencyCode: wallet.CurrencyCode,
			}

			if err := tx.Model(address).
				Where("wallet_id is null").
				Where("currency_code = ?currency_code").
				Limit(1).
				For("update skip locked").
				Select(); err != nil {
					log.Error(err)
					return nil, status.Error(codes.Internal, "Internal error")
			}

			wallet.Addresses[i] = address

			address.WalletId = wallet.ID

			if _, err := tx.Model(address).Column("wallet_id").WherePK().Update(); err != nil {
				log.Error(err)
				return nil, status.Error(codes.Internal, "Internal error")
			}
		}
	}

	if !isOuterTx {
		err = tx.Commit()
	}

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	log.Debugf("Wallet %s successfully created!", in.Name)
	return wallet.ToProto(), nil
}


/*func (s *ApiServer) CreateAddress(ctx context.Context, in *api.CreateAddressRequest) (*api.CryptoAddress, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	wallet := &models.Wallet{ProfileId: ctx.Value("AccountId").(string)}
	db := s.service.DB().WithContext(ctx)
	err := db.Model(wallet).
		Where("profile_id = ?profile_id and id = ?", in.WalletId).
		Column("id", "currency_code").
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	currency := proto.Currency(proto.Currency_value[string(wallet.CurrencyCode)])
	res, err := s.payClients[wallet.CurrencyCode].CreateAddress(
		ctx,
		&payServer.CreateAddressRequest{Currency: currency},
	)

	if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	address := &models.CryptoAddress{
		CurrencyCode: wallet.CurrencyCode,
		WalletId: wallet.ID,
		PrivateKey: res.PrivateKey,
		Address: res.Address,
		Balance: 0,
		PendingBalance: 0,
	}

	if err := db.Insert(address); err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &api.CryptoAddress{
		Address: address.Address,
	}, nil
}*/


func (s *ApiServer) GetWallet(ctx context.Context, in *api.GetWalletRequest) (*api.Wallet, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	wallet := &models.Wallet{ProfileId: ctx.Value("AccountId").(string)}

	if in.WalletId == "" {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	}

	err := db.Model(wallet).
		Where("profile_id = ?profile_id and id = ?", in.WalletId).
		Select()

	if err == pg.ErrNoRows {
		return nil, status.Error(codes.NotFound, "Wallet is not found")
	} else if err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	var addrs []models.CryptoAddress
	if err := db.Model(&addrs).Where("wallet_id = ?", wallet.ID).Select(); err != nil {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	apiAddrs := make([]*api.CryptoAddress, len(addrs))
	for i, addr := range addrs {
		apiAddrs[i] = &api.CryptoAddress{
			Address: addr.Address,
		}
	}

	wp := wallet.ToProto()
	wp.Addresses = apiAddrs

	return wp, nil
}

func (s *ApiServer) GetWallets(ctx context.Context, in *api.GetWalletsRequest) (*api.Wallets, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	db := s.service.DB().WithContext(ctx)
	profileId := ctx.Value("AccountId").(string)

	var wallets []models.Wallet
	err := db.Model(&wallets).
		Where("profile_id = ?", profileId).
		Column("w.*", "Addresses", "Payments").
		Relation("Addresses").
		Relation("Payments", func(q *orm.Query) (*orm.Query, error) {
			return q.Where("pm.status = 'Pending'"), nil
		}).
		Select()

	if err != nil && err != pg.ErrNoRows  {
		log.Error(err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	apiWallets := make([]*api.Wallet, len(wallets))
	for i, wallet := range wallets {
		apiAddrs := make([]*api.CryptoAddress, len(wallet.Addresses))
		for i, addr := range wallet.Addresses {
			apiAddrs[i] = &api.CryptoAddress{
				Address: addr.Address,
			}
		}

		wp := wallet.ToProto()
		wp.Addresses = apiAddrs
		apiWallets[i] = wp
	}

	return &api.Wallets{Wallets: apiWallets} , nil
}
